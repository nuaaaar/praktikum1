package com.yanuarfabien_10191089.praktikum;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.widget.TextView;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView myTextView = (TextView) findViewById(R.id.helloWorldTextView);
        myTextView.setText(Html.fromHtml("Hello <b>My World</b>"));
        getSupportActionBar().setTitle("Yanuar Fabien - Praktikum 1");
    }
}